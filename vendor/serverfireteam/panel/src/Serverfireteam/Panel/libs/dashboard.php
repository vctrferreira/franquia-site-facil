<?php
namespace Serverfireteam\Panel\libs;


class dashboard
{    
    
    public static $urls; 
    
    public static function create()
    {
        self::$urls = \Config::get('panel.panelControllers');
         
        $config    = [
            array(
                'title'   => 'Site builder',
                'count'   => '',
                'showListUrl' => 'panel/site/tutorial',
                'addUrl'      => 'panel/site/create',
            ), 
            array(
                'title'   => 'App builder',
                'count'   => '',
                'showListUrl' => 'panel/appbuilder/tutorial',
                'addUrl'      => 'panel/appbuilder/create',
            ), 
            array(
                'title'   => 'E-Marketing',
                'count'   => '',
                'showListUrl' => 'panel/marketing/tutorial',
                'addUrl'      => 'panel/marketing/create',
            )

        ];
        $dashboard = array();

        // Make Dashboard Items
        

	return $config;
    }
}
