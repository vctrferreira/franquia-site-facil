<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class sf_usuarios extends Model
{
    //
    public function telefone(){
    	return $this->hasOne('App\sf_informacoespessoais', 'id_usuario');
    }
}
