<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SfPessoaFisica extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('sf_pessoaFisica',function($table){
            $table->increments('id');
            $table->integer('id_endereco')->nullable();
            $table->integer('cpf');
            $table->integer('rg');
            $table->string('nome');
            $table->string('sobrenome');
            
            $table->timestamps();



        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('sf_pessoaFisica');
    }
}
