<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SfPlanos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::create('sf_planos',function($table){
            $table->increments('id');
            $table->string('nome');
            $table->longText('descricao');
            $table->float('preco');
            $table->integer('duracao');
            $table->boolean('arquivado');
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('sf_planos');
    }
}
