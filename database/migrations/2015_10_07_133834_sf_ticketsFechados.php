<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SfTicketsFechados extends Migration
{
   /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::create('sf_ticketsFechados',function($table){
            $table->increments('id');
            $table->integer('id_ticket')->unsigned();
            $table->date('dataDeFechamento');
            $table->timestamps();
        });

          Schema::table('sf_ticketsFechados', function($table) {
            $table->foreign('id_ticket')->references('id')->on('sf_tickets');
         });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('sf_ticketsFechados');
    }
}
