<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SfPlanosAtivados extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::create('sf_planosAtivados',function($table){
            $table->increments('id');
            $table->integer('id_usuario')->unsigned();
            $table->integer('id_plano')->unsigned();
            $table->date('dataDeAtivacao');
            $table->boolean('arquivado');
            
            $table->timestamps();
        });
          Schema::table('sf_planosAtivados', function($table) {
            $table->foreign('id_usuario')->references('id')->on('sf_usuarios');
            $table->foreign('id_plano')->references('id')->on('sf_planos');
         });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('sf_planosAtivados');
    }
}
