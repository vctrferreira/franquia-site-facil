<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SfInformacoesPessoais extends Migration
{
  /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('sf_informacoesPessoais',function($table){

            $table->integer('id_usuario')->unsigned();
            $table->integer('id_pessoaFisica')->unsigned()->nullable();
            $table->integer('id_pessoaJuridica')->unsigned()->nullable();
            $table->integer('telefone');
            $table->integer('celular');

            $table->timestamps();


        });
        Schema::table('sf_informacoesPessoais', function($table) {
            $table->foreign('id_pessoaFisica')->references('id')->on('sf_pessoaFisica');
            $table->foreign('id_pessoaJuridica')->references('id')->on('sf_pessoaJuridica');
            $table->foreign('id_usuario')->references('id')->on('sf_usuarios');
         });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::drop('sf_informacoesPessoais');
    }
}
