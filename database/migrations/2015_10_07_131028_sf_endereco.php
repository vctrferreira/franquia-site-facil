<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SfEndereco extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::create('sf_endereco',function($table){
            $table->increments('id');
            $table->integer('cep');
            $table->integer('numero');
            $table->string('enderoco');
            $table->string('pais');

            
            $table->timestamps();



        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('sf_endereco');
    }
}
