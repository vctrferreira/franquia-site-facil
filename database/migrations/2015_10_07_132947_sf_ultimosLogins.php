<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SfUltimosLogins extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::create('sf_ultimosLogins',function($table){
            $table->increments('id');
            $table->integer('id_usuario')->unsigned();
            $table->date('data');
            $table->boolean('status');
            
            $table->timestamps();
        });
         Schema::table('sf_ultimosLogins', function($table) {
            $table->foreign('id_usuario')->references('id')->on('sf_usuarios');
         });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('sf_ultimosLogins');
    }
}
