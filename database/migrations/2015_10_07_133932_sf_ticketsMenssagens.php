<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SfTicketsMenssagens extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sf_ticketsMenssagens', function($table){
            $table->increments('id');
            $table->integer('id_usuario')->unsigned();
            $table->integer('id_ticket')->unsigned();
            $table->longText('menssagem');
            
            $table->timestamps();
            

        });

         Schema::table('sf_ticketsMenssagens', function($table) {
            $table->foreign('id_usuario')->references('id')->on('sf_usuarios');
            $table->foreign('id_ticket')->references('id')->on('sf_tickets');
         });
        //
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
