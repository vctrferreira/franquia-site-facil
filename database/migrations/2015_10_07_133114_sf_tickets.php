<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SfTickets extends Migration
{
     /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::create('sf_tickets',function($table){
            $table->increments('id');
            $table->integer('id_usuario')->unsigned();
            $table->string('assunto');
            $table->string('categoria');
            $table->date('data');
            $table->longText('descricao');
            $table->integer('prioridade');

            $table->boolean('arquivado');
            
            $table->timestamps();
        });
          Schema::table('sf_tickets', function($table) {
            $table->foreign('id_usuario')->references('id')->on('sf_usuarios');
         });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('sf_tickets');
    }
}
