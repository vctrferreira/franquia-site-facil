<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SfPessoaJuridica extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::create('sf_pessoaJuridica',function($table){
            $table->increments('id');
            $table->integer('id_endereco')->nullable();
            $table->integer('cnpj');
            $table->string('nomeFantasia');
            $table->string('nomeEmpresarial');
            $table->string('nomeEmpresario');
            
            $table->timestamps();



        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('sf_pessoaJuridica');
    }
}
