<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SfPagos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sf_pagos',function($table){
            $table->increments('id');
            $table->integer('id_usuario')->unsigned();
            $table->integer('id_plano')->unsigned();
            $table->float('valorPago');
            
            $table->date('dataAbertura');
            $table->date('dataDeConfirmacao');


            $table->string('formaDePagamento');
            $table->boolean('arquivado');
            
            $table->timestamps();

            



        });
        Schema::table('sf_pagos', function($table) {
            $table->foreign('id_plano')->references('id')->on('sf_planos');
            $table->foreign('id_usuario')->references('id')->on('sf_usuarios');
         });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('sf_pagos');
    }
}
